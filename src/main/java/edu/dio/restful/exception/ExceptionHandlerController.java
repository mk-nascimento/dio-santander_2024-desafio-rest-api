package edu.dio.restful.exception;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import edu.dio.restful.dto.ErrorResponse;

@RestControllerAdvice
public class ExceptionHandlerController {
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorResponse> handleDataException(DataIntegrityViolationException exc) {
        ErrorResponse error = new ErrorResponse(exc.getMessage());
        return ResponseEntity.badRequest().body(error);
    }

    @ExceptionHandler(RestfulException.class)
    public ResponseEntity<ErrorResponse> handleBusinessException(RestfulException exc) {
        ErrorResponse error = new ErrorResponse(exc.getMessage());
        return ResponseEntity.unprocessableEntity().body(error);
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ErrorResponse> handleUnexpectedException(Throwable exc) {
        ErrorResponse error = new ErrorResponse(exc.getMessage());
        return ResponseEntity.internalServerError().body(error);
    }
}
