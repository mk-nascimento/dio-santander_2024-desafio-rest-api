package edu.dio.restful.exception;

public class RestfulException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public RestfulException(String message) {
        super(message);
    }
}
