package edu.dio.restful.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.dio.restful.model.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
}
