package edu.dio.restful.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.dio.restful.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
