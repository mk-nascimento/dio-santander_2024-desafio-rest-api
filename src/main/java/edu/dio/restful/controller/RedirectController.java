package edu.dio.restful.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import io.swagger.v3.oas.annotations.Hidden;

@Hidden
@RestController
public class RedirectController {
    @GetMapping
    public RedirectView redirectView() {
        return new RedirectView("/swagger.html");
    }
}
