package edu.dio.restful.controller;

import java.net.URI;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import edu.dio.restful.model.Customer;
import edu.dio.restful.model.Item;
import edu.dio.restful.model.Order;
import edu.dio.restful.service.CustomerService;
import edu.dio.restful.service.ItemService;
import edu.dio.restful.service.OrderService;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@CrossOrigin
@RestController
@RequestMapping("api")
@Tag(name = "Order")
public class OrderController {
    private CustomerService customerService;
    private OrderService orderService;
    private ItemService itemService;

    public OrderController(CustomerService customer, OrderService order, ItemService item) {
        this.customerService = customer;
        this.orderService = order;
        this.itemService = item;
    }

    @ApiResponse(responseCode = "201")
    @PostMapping("/customer/{customerId}/orders")
    public ResponseEntity<Order> create(@PathVariable Long customerId, @RequestBody Order entity) {
        Customer customer = customerService.readUnique(customerId);
        entity.setCustomer(customer);

        Order instance = orderService.create(entity);
        for (Item item : instance.getItems()) {
            item.setOrder(instance);
            itemService.create(item);
        }

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(instance.getId()).toUri();
        return ResponseEntity.created(location).body(instance);
    }

    @GetMapping("/customer/{customerId}/orders")
    public ResponseEntity<List<Order>> read(@PathVariable Long customerId) {
        Customer customer = customerService.readUnique(customerId);

        List<Order> instances = orderService.readByCustomer(customer);
        return ResponseEntity.ok(instances);
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Order> readUnique(@PathVariable Long id) {
        Order instance = orderService.readUnique(id);
        return ResponseEntity.ok(instance);
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        orderService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
