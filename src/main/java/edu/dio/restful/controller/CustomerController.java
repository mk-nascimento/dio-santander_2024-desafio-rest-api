package edu.dio.restful.controller;

import java.net.URI;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import edu.dio.restful.dto.CustomerRequest;
import edu.dio.restful.model.Customer;
import edu.dio.restful.service.CustomerService;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@CrossOrigin
@RestController
@RequestMapping("api")
@Tag(name = "Customer")
public class CustomerController {
    private CustomerService service;

    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @ApiResponse(responseCode = "201")
    @PostMapping("/customer")
    public ResponseEntity<Customer> create(@RequestBody CustomerRequest entity) {
        Customer instance = service.create(entity.toCustomer());
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(instance.getId()).toUri();
        return ResponseEntity.created(location).body(instance);
    }

    @GetMapping("/customer")
    public ResponseEntity<List<Customer>> read() {
        List<Customer> instances = service.read();
        return ResponseEntity.ok(instances);
    }

    @GetMapping("customer/{id}")
    public ResponseEntity<Customer> readUnique(@PathVariable Long id) {
        Customer instance = service.readUnique(id);
        return ResponseEntity.ok(instance);
    }

    @PatchMapping("customer/{id}")
    public ResponseEntity<Customer> update(@PathVariable Long id, @RequestBody CustomerRequest entity) {
        Customer instance = service.update(id, entity.toCustomer());
        return ResponseEntity.ok(instance);
    }

    @DeleteMapping("customer/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
