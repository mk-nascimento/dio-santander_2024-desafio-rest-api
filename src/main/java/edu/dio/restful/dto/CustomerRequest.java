package edu.dio.restful.dto;

import edu.dio.restful.model.Customer;
import lombok.Data;

@Data
public class CustomerRequest {
    private String name;
    private String email;

    public Customer toCustomer() {
        return new Customer(this.name, this.email);
    }
}
