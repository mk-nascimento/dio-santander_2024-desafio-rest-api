package edu.dio.restful.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import edu.dio.restful.model.Customer;
import edu.dio.restful.model.Order;

public interface OrderService extends BaseService<Order, Long> {
    @Transactional(readOnly = true)
    public List<Order> readByCustomer(Customer customer);
}
