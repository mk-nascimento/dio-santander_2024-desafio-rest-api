package edu.dio.restful.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

public interface BaseService<T, ID> {
    @Transactional
    T create(T entity);

    @Transactional(readOnly = true)
    List<T> read();

    @Transactional(readOnly = true)
    T readUnique(ID id);

    @Transactional
    T update(ID id, T entity);

    @Transactional
    void delete(ID id);
}
