package edu.dio.restful.service;

import edu.dio.restful.model.Item;

public interface ItemService extends BaseService<Item, Long> {
}
