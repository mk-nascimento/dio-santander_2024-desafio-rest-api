package edu.dio.restful.service.impl;

import org.springframework.stereotype.Service;

import edu.dio.restful.model.Item;
import edu.dio.restful.repository.ItemRepository;
import edu.dio.restful.service.ItemService;

@Service
public class ItemServiceImpl extends BaseServiceImpl<Item, Long> implements ItemService {
    protected ItemServiceImpl(ItemRepository repository) {
        super(repository);
    }
}
