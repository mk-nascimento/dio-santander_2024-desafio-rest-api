package edu.dio.restful.service.impl;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;

import edu.dio.restful.exception.RestfulException;
import edu.dio.restful.utils.Utils;

public abstract class BaseServiceImpl<T, ID> {
    private final JpaRepository<T, ID> repository;
    private static final HttpStatus NOT_FOUND = HttpStatus.NOT_FOUND;
    private static final String MESSAGE = "Resource ".concat(NOT_FOUND.getReasonPhrase());

    protected BaseServiceImpl(JpaRepository<T, ID> repository) {
        this.repository = repository;
    }

    protected JpaRepository<T, ID> getRepository() {
        return this.repository;
    }

    public T create(T entity) {
        return repository.save(entity);
    }

    public List<T> read() {
        return repository.findAll();
    }

    public T readUnique(ID id) throws RestfulException {
        return repository.findById(id).orElseThrow(
                () -> new RestfulException(MESSAGE.formatted(id)));
    }

    public T update(ID id, T entity) throws RestfulException {
        T instance = repository.findById(id).orElseThrow(
                () -> new RestfulException(MESSAGE.formatted(id)));

        Utils.copyNonNull(entity, instance);

        return repository.save(instance);
    }

    public void delete(ID id) {
        T instance = repository.findById(id).orElseThrow(
                () -> new RestfulException(MESSAGE.formatted(id)));
        repository.delete(instance);
    }
}
