package edu.dio.restful.service.impl;

import org.springframework.stereotype.Service;

import edu.dio.restful.model.Customer;
import edu.dio.restful.repository.CustomerRepository;
import edu.dio.restful.service.CustomerService;

@Service
public class CustomerServiceImpl extends BaseServiceImpl<Customer, Long> implements CustomerService {
    protected CustomerServiceImpl(CustomerRepository repository) {
        super(repository);
    }
}
