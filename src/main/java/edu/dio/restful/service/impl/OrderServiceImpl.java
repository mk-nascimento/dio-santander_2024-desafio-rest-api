package edu.dio.restful.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import edu.dio.restful.model.Customer;
import edu.dio.restful.model.Order;
import edu.dio.restful.repository.OrderRepository;
import edu.dio.restful.service.OrderService;

@Service
public class OrderServiceImpl extends BaseServiceImpl<Order, Long> implements OrderService {
    protected OrderServiceImpl(OrderRepository repository) {
        super(repository);
    }

    public List<Order> readByCustomer(Customer customer) {
        return ((OrderRepository) getRepository()).findByCustomer(customer);
    }
}
