package edu.dio.restful.service;

import edu.dio.restful.model.Customer;

public interface CustomerService extends BaseService<Customer, Long> {
}
