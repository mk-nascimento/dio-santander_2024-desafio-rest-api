# RESTful API com Java e Spring Boot

Este projeto é uma API Restful desenvolvida com Spring Boot 3+, Java 21+, PostgreSQL e Swagger para documentação.
Ele contém modelos básicos e relações entre `Customer`, `Order` e `Item`.

## Tecnologias Utilizadas

- Java 21
- Spring Boot 3.2.5
- Maven
- Lombok
- H2 Database
- PostgreSQL Database
- Spring Data JPA (Java Persistence API)
- DevContainer (Ambiente de desenvolvimento no VS Code)

## Diagrama de Modelos

```mermaid
classDiagram
    Customer "1" --> "N" Order
    Order "1" --> "N" Item
    class Customer {
        +Long id
        +String name
        +String email
    }
    class Order {
        +Long id
        +Date orderDate
        +Customer customer
    }
    class Item {
        +Long id
        +String name
        +int quantity
        +Order order
    }
```

## Configuração do PostgreSQL

Crie um banco de dados PostgreSQL. Em seguida, crie um arquivo `.env` na raiz do projeto baseado no
arquivo `.env.example`.

- Copie o conteúdo de `.env.example` para um novo arquivo `.env` e atualize as credenciais conforme necessário:

    ```bash
    cp .env.example .env
    ```

## Executando o Projeto

### Passos para Execução

1. Clone o repositório:
   ```sh
   git clone https://gitlab.com/mk-nascimento/dio-santander_2024-desafio-rest-api
   cd dio-santander_2024-desafio-rest-api
   ```

2. Compile e execute o projeto usando Maven:

   - Em sistemas Unix-like (Linux, macOS):
        ```bash
        ./mvnw spring-boot:run
        ```

   - Em sistemas Windows:
        ```PowerShell
        mvnw.cmd spring-boot:run
        ```

A aplicação estará disponível em `http://localhost:8080`.

## Endpoints da API

### Customers

- `GET /api/customers`: Retorna todos os clientes.
- `POST /api/customers`: Cria um novo cliente.
- `GET /api/customers/{id}`: Retorna um cliente pelo ID.
- `PATCH /api/customers/{id}`: Atualiza um cliente existente.
- `DELETE /api/customers/{id}`: Deleta um cliente pelo ID.

### Orders

- `GET /api/customer/{customerId}/orders`: Retorna todos os pedidos.
- `POST /api/customer/{customerId}/orders`: Cria um novo pedido.
- `GET /api/orders/{id}`: Retorna um pedido pelo ID.
- `DELETE /api/orders/{id}`: Deleta um pedido pelo ID.

## Referências

- [Spring Boot Documentation](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/)
- [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/3.2.5/reference/htmlsingle/index.html#using.devtools)
- [Spring Data JPA](https://docs.spring.io/spring-boot/docs/3.2.5/reference/htmlsingle/index.html#data.sql.jpa-and-spring-data)
- [Spring Web](https://docs.spring.io/spring-boot/docs/3.2.5/reference/htmlsingle/index.html#web)
- [Maven Documentation](https://maven.apache.org/guides/index.html)
- [H2 Database Documentation](http://www.h2database.com/html/main.html)
- [PostgreSQL Documentation](https://www.postgresql.org/docs/current/)
